function anim(object, newPage, inDir, outDir)
{
	$(object).removeClass().addClass('slideOut' + outDir + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
	{
		$(object).load(newPage, function()
		{
			$(object).removeClass();
			$(object).removeClass().addClass('slideIn' + inDir +  ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
			{
				$(object).removeClass();
			});
		});
	});
}

function animOut(object, outDir, callback)
{
	$(object).addClass('slideOut' + outDir + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
	{
		if(typeof callback == 'function')
		{
			callback();
		}
	});
}

function animIn(object, newPage, inDir, callback)
{
	$(object).load(newPage, function()
	{
		$(object).addClass('slideIn' + inDir +  ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
		{
			$(object).removeClass('slideIn' + inDir +  ' animated');
			if(typeof callback == 'function')
			{
				callback();
			}
		});
	});
}

var allPages;

var NORTH;
var SOUTH;
var EAST;
var WEST;

var currentLoc;

function init()
{
	allPages = new Array();

	NORTH = 'N';
	SOUTH = 'S';
	EAST = 'E';
	WEST = 'W';

	currentLoc = new Array();
	currentLoc[0] = 0;
	currentLoc[1] = 0;
}

/*
*currentLoc[0] is X
*currentLoc[1] is Y
*/

function addPage(page, locX, locY)
{
	if(locX < 0 || locY < 0)
	{
		return -1;
	}
	if(typeof allPages[locX] != 'undefined')
	{
		allPages[locX][locY] = page;
	}
	else
	{
		allPages[locX] = new Array();
		allPages[locX][locY] = page;
	}
}

function scroll(contentElement, direction)
{
	if(direction == WEST)
	{
		if(typeof allPages[currentLoc[0] - 1] == 'undefined' || typeof allPages[currentLoc[0] - 1][currentLoc[1]] == 'undefined')
		{
			anim(contentElement, allPages[currentLoc[0]][currentLoc[1]], "Right", "Left");
		}
		else
		{
			anim(contentElement, allPages[currentLoc[0] - 1][currentLoc[1]], "Right", "Left");
			currentLoc[0] = currentLoc[0] - 1;
		}
	}
	else if(direction == EAST)
	{
		if(typeof allPages[currentLoc[0] + 1] == 'undefined' || typeof allPages[currentLoc[0] + 1][currentLoc[1]] == 'undefined')
		{
			anim(contentElement, allPages[currentLoc[0]][currentLoc[1]], 'Left', 'Right');
		}else
		{
			anim(contentElement, allPages[currentLoc[0] + 1][currentLoc[1]], 'Left', 'Right');
			currentLoc[0] = currentLoc[0] + 1;
		}

	}
	else if(direction == SOUTH)
	{
		if(typeof allPages[currentLoc[0]] == 'undefined' || typeof allPages[currentLoc[0]][currentLoc[1] + 1] == 'undefined')
		{
			anim(contentElement, allPages[currentLoc[0]][currentLoc[1]], 'Up', 'Down');
		}
		else
		{
			anim(contentElement, allPages[currentLoc[0]][currentLoc[1] + 1], 'Up', 'Down');
			currentLoc[1] = currentLoc[1] + 1;
		}
	}
	else if(direction == NORTH)
	{
		if(typeof allPages[currentLoc[0]] == 'undefined' || typeof allPages[currentLoc[0]][currentLoc[1] + 1] == 'undefined')
		{
			anim(contentElement, allPages[currentLoc[0]][currentLoc[1]], 'Up', 'Down');
		}
		else
		{
			anim(contentElement, allPages[currentLoc[0]][currentLoc[1] - 1], 'Up', 'Down');
			currentLoc[1] = currentLoc[1] - 1;
		}
	}
}

function scrollToPage(contentElement, page)
{
	var pageLoc = [];
	pageLoc[0] = -1;
	pageLoc[1] = -1;
	for(var i = 0; i < allPages.length; i++)
	{
		var q = allPages[i].indexOf(page);
		if(q != -1)
		{
			pageLoc[0] = i;
			pageLoc[1] = q;
			break;
		}
	}
	if(pageLoc[0] != -1 && pageLoc[1] != -1)
	{
		var toMove = findDirectionToScroll(currentLoc[0], currentLoc[1], pageLoc[0], pageLoc[1]);
		if(toMove.indexOf(NORTH) != -1)
		{
			//anim(contentElement, page, 'Down', 'Up');
			animOut(contentElement, 'Up', function()
			{
				if(toMove.indexOf(WEST) != -1)
				{
					$(contentElement).removeClass('slideOut' + 'Up' + ' animated');
					animIn(contentElement, page, 'Right');
				}
				else if(toMove.indexOf(EAST) != -1)
				{
					$(contentElement).removeClass('slideOut' + 'Up' + ' animated');
					animIn(contentElement, page, 'Left');
				}
				else
				{
					$(contentElement).removeClass('slideOut' + 'Up' + ' animated');
					animIn(contentElement, page, 'Down')
				}
			});	
		}
		else if(toMove.indexOf(SOUTH) != -1)
		{
			//anim(contentElement, page, 'Up', 'Down');
			animOut(contentElement, 'Down', function()
			{
				if(toMove.indexOf(WEST) != -1)
				{
					$(contentElement).removeClass('slideOut' + 'Down' + ' animated');
					animIn(contentElement, page, 'Right');
				}
				else if(toMove.indexOf(EAST) != -1)
				{
					$(contentElement).removeClass('slideOut' + 'Down' + ' animated');
					animIn(contentElement, page, 'Left');
				}
				else
				{
					$(contentElement).removeClass('slideOut' + 'Down' + ' animated');
					animIn(contentElement, page, 'Up')
				}
			});	
		}
		else if(toMove.indexOf(EAST) != -1)
		{
			anim(contentElement, page, 'Left', 'Right');
		}
		else if(toMove.indexOf(WEST) != -1)
		{
			anim(contentElement, page, 'Right', 'Left');
		}
	}
}

function findDirectionToScroll(fromX, fromY, toX, toY)
{
	var returnValue = '';
	/*
	* If set to 1, you will be going North. If set to -1, you will be going SOUTH. If 0, you are not changing on this axis
	*/
	var NORTHSOUTH = 0;
	/*
	* If set to 1, you will be going EAST. If set to -1, you will be going WEST. If set to 0, you are not changing on this axis.
	*/
	var EASTWEST = 0;

	if(fromX < toX)
	{
		EASTWEST = 1; //Going East
	}
	else if(fromX > toX)
	{
		EASTWEST = -1; //Going West
	}
	if(fromY < toY)
	{
		NORTHSOUTH = 1; //Going North
	}
	else if(fromY > toY)
	{
		NORTHSOUTH = -1; //Going South
	}
	if(NORTHSOUTH != 0)
	{
		returnValue += (NORTHSOUTH == 1) ? NORTH : SOUTH;
	}
	if(EASTWEST != 0)
	{
		returnValue += (EASTWEST == 1) ? EAST : WEST;
	}
	return returnValue;
}

//------------------------------------------------------------------------------
